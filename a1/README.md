> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alex Bustamante

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions

#### README.md file should include the following items:

* Screenshot of ampps installation running 
* Screenshot of running JDK java Hello 
* Screenshot of running Android Studio My First App
* git commands w/short descriptions
* Bitbucket repo links:
    a) This assignment
    b) The completed tutorial repo (bitbucketstationlocations)

> #### Git commands w/short descriptions:

1. git init - turns a directory into a git repository
2. git status - returns current state of the repository, such as if a file has not been commited
3. git add - adds a file into the staging area
4. git commit - saves changes made to files in a local repository
5. git push - sends a local commit to a remote repository 
6. git pull - pulls changes from a remote repository to a local repository
7. git branch - shows what branch a local repository is on or add and delete branches

#### Assignment Screenshots:
|              *Screenshot of AMPPS running http://localhost* :             |                     *Screenshot of running java Hello* :                     | *Screenshot of Android Studio - My First App* :                                    |   |   |
|:-------------------------------------------------------------------------:|:----------------------------------------------------------------------------:|------------------------------------------------------------------------------------|---|---|
| ![AMPPS Installation Screenshot](img/ampps_img.png "A1 AMPPS Screenshot") | ![ JDK Installation Screenshot ] (img/jdk_install.png  "A1 JDK Screenshot" ) | ![Android Studio Installation Screenshot](img/android.png "A1 Android Screenshot") |   |   |
|                                                                           |                                                                              |                                                                                    |   |   |
|                                                                           |                                                                              |                                                                                    |   |   |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/afb19a/bitbucketstationlocations/ "Bitbucket Station Locations")
