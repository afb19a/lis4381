# LIS4381

## Alex Bustamante

### Assignment 4 Requirements:

*Four-Parts:*

1. Create Bootstrap carousel to go into different assignments
2. Create a form to collect data and perform client-side validation
3. Link to a local lis4381 web app
4. Skill sets 10-12

#### README.md file should include the following items:

* Screenshot of carousel
* Screenshot of Assignment 4 invalid data entry
* Screenshot of Assignment 4 valid data entry
* Local lis4381 link

#### Assignment Screenshots:

| Screenshot of carousel:                                  | Screenshot of Assignment 4 invalid data entry: | Screenshot of Assignment 4 valid data entry:  |
|----------------------------------------------------------|------------------------------------------------|-----------------------------------------------|
| ![LIS4381 Portal (Main Page) Screenshot](img/a4s1.1.png) | ![Failed Validation Screenshot](img/a4s2.png)  | ![Passed Validation Screenshot](img/a4s3.png) |


#### Skill Sets Screenshots:

| Skillset 10:                 | Skillset 11:                 | Skillset 12:                 |
|------------------------------|------------------------------|------------------------------|
| ![Skillset 10](img/ss10.png) | ![Skillset 11](img/ss11.png) | ![Skillset 12](img/ss12.png) |

#### Link
[Local LIS4381](http://localhost/repos/lis4381/index.php)