<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that showcases my work with Mobile Web Application Development.">
		<meta name="author" content="Alex Bustamante">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Began creating My First App to familiarize myself with Android Studio, as well as with design principles and researching XML functions on my own. Includes screenshots of running application's first user interface; screenshot of running application's second user interface; screenshot of Java Skill sets 1 - 3. </p>

				<h4>Screenshot of First User Interface Running</h4>
				<img src="img/android_screen1.png" class="img-responsive center-block" alt="My First Project Screen 1">

				<h4>Screenshot of Second User Interface Running</h4>
				<img src="img/android_screen2.png" class="img-responsive center-block" alt="My First Project Screen 2">

				<h4>Even or Odd Skillset Screenshot</h4>
				<img src="img/skillset1.png" class="img-responsive center-block" alt="Skill set 1">

				<h4>Largest Number Skillset Screenshot</h4>
				<img src="img/skillset2.png" class="img-responsive center-block" alt="Skill set 2">
				
				<h4>Arrays and Loops Skillset Screenshot</h4>
				<img src="img/skillset3.png" class="img-responsive center-block" alt="Skill set 3">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
