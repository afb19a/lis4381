> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alex Bustamante

### Assignment 2 Requirements:

*Three Parts:*

1. Healthy Recipe Mobile App
2. Chapter Questions (Chs 3,4)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshots of running mobile app
* Screenshot of Java skill sets

#### Assignment Screenshots:

*Screenshots of running Healthy Recipe Mobile App:*

| Screen 1:                                                                | Screen 2:                                                                      |
|--------------------------------------------------------------------------|--------------------------------------------------------------------------------|
| ![HealthyRecipesScreenshot1] (img/android_screen1.png  “Main Screen” ) | ![HealthyRecipesScreenshot2] (img/android_screen2.png   “Ingredient Screen” ) |

*Screenshots of Skillsets:*

| Skill Set 1: Even Or Odd                         | Skill Set 2: Larger of Two Numbers               | Skill Set 3: Arrays and Loops                    |
|--------------------------------------------------|--------------------------------------------------|--------------------------------------------------|
| ![SkillSet1] (img/skillset1.png  "Skill Set 1") | ![SkillSet2] (img/skillset2.png  "Skill Set 2") | ![SkillSet3] (img/skillset3.png  "Skill Set 3") |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/afb19a/bitbucketstationlocations/ "Bitbucket Station Locations")


