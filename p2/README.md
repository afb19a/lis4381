# LIS4381 - Mobile Web Application Development

## Alex Bustamante

### Project 2 Requirements:

*Four parts:*

1. Basic server-side validation
2. Add edit function
3. Add delte function
4. Create RSS feed file

#### README.md file should include the following items:

* Project 2 petstore tables
* Edit petstore table entry
* Edit petstore table error
* Home page with carousel images
* RSS Feed
* Bitbucket repo link

#### Assignment Screenshots:

| Carousel Screenshot                      | index.php Screenshot                       |
|------------------------------------------|--------------------------------------------|
| ![Carousel Screenshot](img/carousel.png) | ![index.php Screenshot](img/indexAlex.png) |

| edit_petstore.php Screenshot                               | failed validation Screenshot                                   |
|------------------------------------------------------------|----------------------------------------------------------------|
| ![edit_petstore.php Screenshot](img/edit_petstoreAlex.png) | ![failed validation Screenshot](img/failed_validationAlex.png) |

| passed validation Screenshot                                   | delete record prompt                                   |
|----------------------------------------------------------------|--------------------------------------------------------|
| ![passed validation Screenshot](img/passed_validationAlex.png) | ![delete record prompt](img/failed_validationAlex.png) |

| successfully deleted record Screenshot                       | RSS Feed                 |
|--------------------------------------------------------------|--------------------------|
| ![successfully deleted record Screenshot](img/indexAlex.png) | ![RSS Feed](img/RSS.png) |


#### Link

[Local LIS4381](http://localhost/repos/lis4381/index.php)
