> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alex Bustamante

### LIS4381 Requirements:

*Assignment Links*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations)
    * Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Create Healthy Recipes Android App
    * Provide screenshots of completed app
    * Provide screenshots of completed java skill sets 
3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Create ERD with MySQL Workbench
        * Add 10 records to each table
        * Provide Screenshots of ERD
    * Create MyEvent Application
        * Provide screenshots of first and second user interface
    * Provide links to a3.mwb and a3.sql
    * Provide screenshots of completed java skill sets
4. [A4 README.md](a4/README.md "My A4 README.md file")
    * Create a Bootstrap carousel
    * Provide screenshots for the carousel and data validation being valid and invalid
    * Provide screenshots of completed Java skill sets
5. [A5 README.md](a5/README.md "My A5 README.md file")
    * Develop server-side validation of a form in php
    * Develop a form that adds data into a database
    * Provide screenshots of completed Java skill sets

*Project Links*

1. [P1 README.md](p1/README.md "My P1 README.md file")
    * Backwards engineer business card application
    * Provide Screenshots of completed Java skill sets
2. [P2 README.md](p2/README.md "My P2 README.md file")
    * Basic server-side validation
    * Add edit function
    * Add delete function
    * Create RSS feed file 
