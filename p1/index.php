<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that showcases my work with Mobile Web Application Development.">
		<meta name="author" content="Alex Bustamante">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
				<strong>Description:</strong> Created a business card application that showcased our photo, contact information, and interests. Includes screenshots of skillsets 7-9. <br><br>
				</p>

				<h4>My Business Card - Activity 1</h4>
				<img src="img/businesscard1.png" class="img-responsive center-block" alt="Activity 1">

				<h4>My Business Card - Activity 2</h4>
				<img src="img/businesscard2.png" class="img-responsive center-block" alt="Activity 2">

				<h4>Random Array Using Methods and Data Validation Skillset</h4>
				<img src="img/SS7.png" class="img-responsive center-block" alt="Skillset 7">

				<h4>Largest of Three Integers Skillset Screenshot</h4>
				<img src="img/SS8.png" class="img-responsive center-block" alt="Skillset 8">
				
				<h4>Array_Runtime_Data_Validation Skillset Screenshot</h4> 
				<img src="img/SS9.png" class="img-responsive center-block" alt="Skillset 9">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>