# LIS4381 - Mobile Web Application Development

## Alex Bustamante

### Project 1 Requirements:

*Sub-Heading:*

1. My Business Card Application
2. Skillsets

#### README.md file should include the following items:

* Screenshot of running Android Studio first and second user interface
* Screenshot of all Java skill sets 7-9 running 

#### Assignment Screenshots:

*Screenshot of Android Studio - My Business Card:*

| My Business Card - Main                                                                                | My Business Card - Details                                                                             |
|--------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|
| ![My Business Card App Screenshot 1](img/businesscard1.png "My Business Card App Screenshot 1") | ![My Business Card App Screenshot 2](img/businesscard2.png "My Business Card App Screenshot 2") |

*Screenshot of Java Skillsets:*

| Skillset 7: Random Array Using Methods and Data Validation | Skillset 8: Largest of Three Integers              | Skillset 9: Array_Runtime_Data_Validation              |
|------------------------------------------------------------|----------------------------------------------------|--------------------------------------------------------|
| ![SS7](img/SS7.png "SS7 Using Methods and Data Validation") | ![SS8](img/SS8.png "SS8 Largest of Three Integers") | ![SS9](img/SS9.png "SS9 Array_Runtime_Data_Validation") |

#### Assignment Links:

*Bitbucket Repository:*
[P1 Bitbucket Repository Link](https://bitbucket.org/afb19a/lis4381/src/ "Bitbucket Class Repository")