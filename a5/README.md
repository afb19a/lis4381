# LIS4381

## Alex Bustamante

### Assignment 5 Requirements:

*Three-parts:*

1. Develop a server-side validation of a form in php
2. Develop a form that adds data into a database
3. Skill sets 13-15

#### README.md file should include the following items:

* Screenshot of index.php for petstore data
* Screenshot of server-side data validation
* Screenshots of skill sets running

#### Assignment Screenshots:

| Index.php:                | Error.php:                 |
|---------------------------|----------------------------|
| ![index](img/list_sc.png) | ![error](img/error_sc.png) |

#### Skill Sets Screenshots:

| Skillset 13                  |
|------------------------------|
| ![Skillset 13](img/ss13.png) |

| Simple Calculator Add               | Simple Calculator Add Process                       |
|-------------------------------------|-----------------------------------------------------|
| ![Calculator Add](img/calc_add.png) | ![Calculator Process](img/calc_add_process.png)     |


| Simple Calculator Division             | Simple Calculator Divide Process                       |
|----------------------------------------|--------------------------------------------------------|
| ![Calculator Divide](img/calc_div.png) | ![Calculator Divide Process](img/calc_div_process.png) |

| PHP Write/Read                            | PHP Write/Read process                              |
|-------------------------------------------|-----------------------------------------------------|
| ![PHP Write/Read](img/readwriteindex.png) | ![PHP Write/Read process](img/readwriteprocess.png) |

#### Link

[Local LIS4381](http://localhost/repos/lis4381/index.php)
