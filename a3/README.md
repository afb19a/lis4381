> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alex Bustamante

### Assignment 3 Requirements:

*Four Parts*

1. Create database for pet store
2. Create Android Application to calculate concert ticket prices
3. Chapter Questions (Chpt 5, 6)
4. Skill sets 4 - 6

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application first user interface
* Screenshot of running application's second user interface
* Link to a3.mwb and a3.sql files 

#### Assignment Screenshots:

*Screenshot of ERD:*

 ![Pet Store ERD Screenshot](img/peterd.png "Pet Store ERD Screenshot")

*Screenshot of My Event App:*

| My Event - Main                                                                      | My Event- Calculated                                                                |
|---------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------|
|  ![My Event App Screenshot 1](img/androidapps1.png "My Event App Screemshot 1") |  ![My Event App Screenshot 2](img/androidapps2.png "My Event App Screenshot 2") |

*Screenshot of Java Skill Sets:*

 Screenshot of Skillset 4: Decision Structures:      | Screenshot of Skillset 5: Random Array:	      | Screenshot of Skillset 6: Methods:      
:---------------------------------------------------:|:---------------------------------------------:|:---------------------------------------:
 ![SS4](img/skillset4.png "SS4 Decision Structures") |  ![SS5](img/skillset5.png "SS5 Random Array") | ![SS6](img/skillset6.png "SS6 Methods") 

#### Assignment Links:

*A3 Docs:*

[a3 mwb file](docs/a3.mwb "A3 MWB File")

[a3 sql file](docs/a3.sql "A3 SQL File")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/afb19a/bitbucketstationlocations/ "Bitbucket Station Locations")
