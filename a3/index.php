<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that showcases my work with Mobile Web Application Development.">
		<meta name="author" content="Alex Bustamante">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
				<strong>Description:</strong> Began to incorporate database solutions into client/server application development. Includes screenshots of skillsets 4-6. Created an application that followed business requirements. The requirements are as follows:<br><br>
                A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record, track, and maintain relevant company data, based upon the following business rules:<br><br>
                <ol class="text-align: left">
                    <li>A customer can buy many pets, but each pet, if purchased, is purchased by only one customer.</li>
                    <li>A store has many pets, but each pet is sold by only one store.</li>
                    <br><br>
                </ol>
                    <strong>Solutions:</strong> <br>
                    <a href="doc/sa3.mwb">A3 MWB</a><br>
                    <a href="docs/a3.sql">A3 SQL</a><br><br>
				</p>

				<h4>Screenshot of ERD</h4>
				<img src="img/peterd.png" class="img-responsive center-block" alt="ERD">

				<h4>First User Interface Running</h4>
				<img src="img/androidapps1.png" class="img-responsive center-block" alt="First User Interface">

				<h4>Second User Interface Running</h4>
				<img src="img/androidapps2.png" class="img-responsive center-block" alt="Second User Interface">

				<h4>Decision Structures Skillset</h4>
				<img src="img/skillset4.png" class="img-responsive center-block" alt="Skillset 4">

				<h4>Methods Skillset Screenshot</h4>
				<img src="img/skillset5.png" class="img-responsive center-block" alt="Skillset 5">
				
				<h4>Other Methods Skillset Screenshot</h4> 
				<img src="img/skillset6.png" class="img-responsive center-block" alt="Skillset 6">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
